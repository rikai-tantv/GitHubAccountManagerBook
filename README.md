# GitHubAccountManagerBook

Display list of users GitHub


# MACOS SYSTEM
- MacOS: 12.0.1
- Xcode: 13.1
- Language: Switf, SwitfUI

-------
![HomeScreen](https://i.imgur.com/sCzxhZI.png)

![Profile](https://i.imgur.com/exaWdHf.png)

## Installation

Use [CocoaPods](https://cocoapods.org).

1. Update your cocoapods local repo
```ruby
pod install
```

2. Run Project

3. Postman API

Import from url: Use [https://www.getpostman.com/collections/235d46f2e7071c19c11b](https://www.getpostman.com/collections/235d46f2e7071c19c11b).

After import please setup enviroment as below:
```ruby
github_base_api = https://api.github.com
```


## Other
I contributed code to a repositories use Storyboard as below

[Map4D](https://github.com/map4d/map4d-ios-sdk).

License
-------

Copyright (C) 2022 RIKAI TECHNOLOGY. All Rights Reserved.
