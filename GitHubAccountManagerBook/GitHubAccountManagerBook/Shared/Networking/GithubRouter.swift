//
//  GithubRouter.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import Foundation

enum GithubRouter: RequestInfoConvertible {
    case listUser
    case getUser(username: String)
    
    var endpoint: String {
        "https://api.github.com"
    }
    
    var urlString: String {
        "\(endpoint)/\(path)"
    }
    
    var path: String {
        switch self {
        case .listUser:
            return "users?since=0&per_page=1000"
        case .getUser(let username):
            return "users/\(username)"
        }
    }
    
    func asRequestInfo() -> RequestInfo {
        let requestInfo: RequestInfo = RequestInfo(url: urlString)
        // Set other property, like headers, parameters for requestInfo here
        return requestInfo
    }
}
