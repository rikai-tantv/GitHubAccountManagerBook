//
//  GithubNetworkProvider.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import Foundation
import Combine

protocol GithubNetworkProvider {
    func getListUser() -> AnyPublisher<[User], Error>
    func getUser(username: String) -> AnyPublisher<User?, Error>
}

class GithubNetworkClient: GithubNetworkProvider {
    var networkClient: NetworkProvider = NetworkClient.instance
    
    func getListUser() -> AnyPublisher<[User], Error> {
        networkClient.request(GithubRouter.listUser).decode()
    }
    
    func getUser(username: String) -> AnyPublisher<User?, Error> {
        networkClient.request(GithubRouter.getUser(username: username)).decode()
    }
}

