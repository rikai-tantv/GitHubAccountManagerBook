//
//  User.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import Foundation

struct User: Codable, Hashable {
    enum CodingKeys: String, CodingKey {
        case login, name, bio, followers, following, location, blog
        case idUser = "id"
        case avatarUrl = "avatar_url"
        case siteAdmin = "site_admin"
        case htmlURL = "html_url"
        case publicRepos = "public_repos"
        case publicGists = "public_gists"
    }
    
    var idUser: Int?
    var login: String?
    var name: String?
    var avatarUrl: String?
    var siteAdmin: Bool?
    var bio: String?
    var location: String?
    var blog: String?
    var htmlURL: URL?
    var publicRepos: Int?
    var publicGists: Int?
    var followers: Int?
    var following: Int?
}

