//
//  ContentView.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/06.
//

import SwiftUI

struct ContentView: View {
    static let username = "at-tantv"
    
    var body: some View {
        NavigationView {
            UserListCoordinator()
        }
    }
}



struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
