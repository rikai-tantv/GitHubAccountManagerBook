//
//  GitHubAccountManagerBookApp.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/06.
//

import SwiftUI

@main
struct GitHubAccountManagerBookApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
