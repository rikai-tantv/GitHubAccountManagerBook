//
//  ProfileView.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import SwiftUI
import SwiftUIRemoteImage

struct ProfileView: View {
    @StateObject private var viewModel = ProfileViewModel()
    let defaultAvatar = "https://i.pinimg.com/474x/ab/9c/41/ab9c4101905fd6b998d7012abee43ec8.jpg"
    
    let username: String
    let tapOnLinkAction: (URL) -> Void
    
    var user: User? {
        viewModel.user
    }
    
    var body: some View {
        VStack {
            VStack {
                ZStack(alignment: .top) {
                    Rectangle()
                        .foregroundColor(Color.white)
                        .edgesIgnoringSafeArea(.top)
                        .frame(height: 150)
                    VStack{
                        RemoteImage(url: user?.avatarUrl ?? defaultAvatar)
                    }
                    .background(Color.red)
                    .font(.headline)
                    .mask(Circle())
                }
                VStack(spacing: 10) {
                    Text(viewModel.username ?? "")
                        .font(.title)
                        .leadingAlignment()
                    
                    Text(viewModel.displayName ?? "")
                        .font(.title2)
                        .leadingAlignment()
                    
                    Text(viewModel.bio ?? "")
                    
                    VStack {
                        Text(viewModel.location ?? "")
                            .leadingAlignment()
                        Text(viewModel.publicReposText ?? "")
                            .leadingAlignment()
                        Text(viewModel.publicGistsText ?? "")
                            .leadingAlignment()
                        Text(viewModel.followersText ?? "")
                            .leadingAlignment()
                        Text(viewModel.followingText ?? "")
                            .leadingAlignment()
                        Text(viewModel.blog ?? "")
                            .leadingAlignment()
                    }
                    
                    Button("Open Github website to see more details") {
                        if let url = user?.htmlURL {
                            tapOnLinkAction(url)
                        }
                    }
                    .leadingAlignment()
                    
                    Spacer()
                }
            }
        }
        .padding()
        .onAppear(perform: {
                viewModel.getUser(username: username)
        }).navigationBarTitle("Profile", displayMode: .inline)
    }
}

