//
//  UserListViewModel.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import Foundation

class UserListViewModel: ObservableObject {
    @Published var users: [User] = []
    var networkClient: GithubNetworkProvider = GithubNetworkClient()
    
    func getListUser() {
        networkClient
            .getListUser()
            .replaceError(with: [])
            .assign(to: &$users)
    }
}
