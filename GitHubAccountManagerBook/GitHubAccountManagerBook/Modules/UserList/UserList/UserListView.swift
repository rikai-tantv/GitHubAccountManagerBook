//
//  UserListView.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import SwiftUI

struct UserListView: View {
    @StateObject var viewModel = UserListViewModel()
    let tapOnUserAction: (User) -> Void
    
    var body: some View {
        List(viewModel.users, id: \.self) { user in
            Button(action: {
                tapOnUserAction(user)
            }, label: {
                UserCell(user: user)
            })
        }
        .onAppear {
            viewModel.getListUser()
        }
    }
}
