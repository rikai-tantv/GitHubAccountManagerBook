//
//  UserCell.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//


import SwiftUI
import SwiftUIRemoteImage

struct UserCell: View {
    let user: User
    let defaultAvatar = "https://i.pinimg.com/474x/ab/9c/41/ab9c4101905fd6b998d7012abee43ec8.jpg"
    var body: some View {
        VStack {
            HStack {
                VStack {
                    RemoteImage(url: user.avatarUrl ?? defaultAvatar)
                }.frame(width: 100, height: 100)
                    .leadingAlignment()
                VStack {
                    Text(user.login ?? "")
                    if user.siteAdmin ?? false {
                        Text("STAFF")
                        .padding(5.0)
                        .font(.system(size: 15))
                        .foregroundColor(.white)
                        .background(Color.blue)
                        .clipShape(RoundedRectangle(cornerRadius: 5))
                    }
                    
                }.leadingAlignment()
            }.leadingAlignment()
        }
    }
}
