//
//  UserListCoordinator.swift
//  GitHubAccountManagerBook
//
//  Created by at-tantv on 2022/05/07.
//

import SwiftUI

struct UserListCoordinator: View {
    @State private var selectedUser: User?
    @State private var isProfilePresented = false
    
    var body: some View {
        VStack {
            UserListView(tapOnUserAction: { user in
                isProfilePresented = true
                selectedUser = user
            })
            .listStyle(PlainListStyle())
            .navigationBarTitle("List Users", displayMode: .inline)
            .navigationBarItems(trailing: Button(action: {
            }, label: {
                Image(systemName: "person.crop.circle")
            }))
            
        }.fullScreenCover(isPresented: $isProfilePresented, content: {
            ProfileCoordinator(username: selectedUser?.login ?? "")
        })
    }

}
